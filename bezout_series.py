import numpy as np
import sympy as sy
from scipy.special import comb
from scipy.special import factorial
from sympy.utilities.iterables import ordered_partitions

from helpers import next_permutation

def e0(n, dtype=np.int):
    """
    Return the unit vector (1,0,...,0) with 1+n components.
    """
    e = np.zeros(1+n, dtype=dtype)
    e[0] = 1
    return list(e)

def dim_Hi(n, d):
    """
    Compute the (complex affine) dimension of the space H_d_i of homogenious polynomials.
    """
    vfunc = np.vectorize(comb)
    N_i = vfunc(n+d, d, exact=True)
    return N_i

def dim_Hd(n, d):
    """
    Compute the (complex affine) dimension of the space H_d of homogenious polynomials.
    """
    N = np.sum(dim_Hi(n, d))
    return N

def symbols(n):
    """
    Generate the symbols x0,...,xn.
    """
    symbols_generator = sy.numbered_symbols('x')
    symbols = list()
    for _ in np.arange(1+n):
        symbols.append(next(symbols_generator))
    return symbols
    #return [next(symbols_generator) for _ in np.arange(1+n)]

def monomial_basis0(symbols, deg):
    """
    Generate a monomial basis of constant degree `deg`.
    """
    n_ = len(symbols) # == n+1
    hom_basis = list()
    for p_ in ordered_partitions(n_+deg, n_):
        p = p_ - np.ones(n_, dtype=np.int64)
        hom_basis.append(sy.Poly({tuple(p): 1}, symbols))
        while next_permutation(p):
            hom_basis.append(sy.Poly({tuple(p): 1}, symbols))
    sorted_hom_basis = sorted(hom_basis, key=sy.polys.orderings.monomial_key('grevlex', symbols))
    return sorted_hom_basis

def monomial_basis1(symbols, deg):
    dim = dim_Hd(len(symbols)-1, deg)
    aff_basis = sorted(sy.itermonomials(symbols, deg), key=sy.polys.orderings.monomial_key('grevlex', symbols))
    hom_basis = aff_basis[-dim:]
    return [sy.Poly(m, symbols) for m in hom_basis]

def monomial_basis2(symbols, deg):
    monomials1 = sy.polys.monomials.itermonomials(symbols, deg)
    monomials2 = sy.polys.monomials.itermonomials(symbols, deg-1)
    hom_basis = monomials1 - monomials2
    sorted_hom_basis = sorted(hom_basis, key=sy.polys.orderings.monomial_key('grevlex', symbols))
    return [sy.Poly(m, symbols) for m in sorted_hom_basis]

def monomial_basis3(symbols, deg):
    aff_basis = list(sy.Poly(m, symbols) for m in sy.itermonomials(symbols, deg))
    hom_basis = list(filter(lambda m: m.total_degree() == deg, aff_basis))
    sorted_hom_basis = sorted(hom_basis, key=sy.polys.orderings.monomial_key('grevlex', symbols))
    return sorted_hom_basis

monomial_basis = monomial_basis0 # the fastest one

def polynomial_from_dense(symbols, deg, coeffs):
    """
    Convert a dense encoding of a homogeneous polynomial, `coeffs`,
    of degree `deg` into a sympy polynomial.
    """
    # validate input
    Nd = dim_Hd(len(symbols)-1, deg)
    if Nd != len(coeffs):
        raise ValueError("There must be exactly Nd coeffs.")
    # compute polynomial
    basis = monomial_basis(symbols, deg)
    polynomial = np.inner(coeffs, basis)
    return polynomial

def random_polynomial(symbols, deg):
    """
    Generate a random homogeneous polynomial of degree `deg`.
    """
    Nd = dim_Hd(len(symbols)-1, deg)
    coeffs = 2*np.random.rand(Nd) + 2.j*np.random.rand(Nd) - (1+1.j)
    return polynomial_from_dense(symbols, deg, coeffs)

def evaluate(f, x, dtype=np.cfloat):
    """
    Evaluate the system of polynomials `f` at `x`; `x` in C^{1+n}.
    """
    return np.array([f_.eval(x) for f_ in f], dtype=dtype)

def canonical_norm(f):
    """
    Compute the canonical norm of a polynomial `f` in C^N.
    """
    return np.sum(np.sum(np.absolute(f_.coeffs()) ** 2) for f_ in f) ** 0.5

def bombieri_weyl_norm(f):
    """
    Compute the norm of a polynomial `f` induced by the Bombieri-Weyl or Kostlan inner product.
    """
    norm = np.float(0)
    for f_ in f:
        f_dict = f_.as_dict()
        for (alpha, coeff) in f_dict.items():
            d = np.sum(alpha, dtype=int)
            norm += (np.absolute(coeff) ** 2) * np.prod(factorial(alpha)) / factorial(d)
    return norm ** 0.5
