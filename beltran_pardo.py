import numpy as np
import sympy as sy
import bezout_series as bs
from scipy.misc import factorial

def random_t():
    """
    Draw a random `t` to be used in the construction of an efficient initial pair, $t \in [0,1]$.
    """
    t = np.random.rand()
    return t

def random_h(symbols, d):
    """
    Draw a random `h` to be used in the construction of an efficient initial pair, $h \in L_{e_0}^\perp$.
    """
    n = len(symbols)-1
    h = [bs.random_polynomial(symbols, deg) for deg in d]
    # remove $x_0^{d_i}$ and the $x_j$-linear parts for $j > 0$:
    for i, h_ in enumerate(h):
        if d[i] == 1:
            h[i] = sy.Poly(0, symbols)
        else:
            h_dict = h_.as_dict()
            # construct a reusable multi-index:
            alpha = np.array(bs.e0(n), dtype=int) * (d[i]-1)
            for j in range(1+n):
                alpha[j] += 1
                h_dict[tuple(alpha)] = 0
                alpha[j] -= 1
            h[i] = sy.Poly(h_dict, symbols)
    return h


def random_M(n):
    """
    Draw a random matrix compatible with the M used to compute efficient initial pairs.
    Currently, the distribution is not aware that M should be drawn from the unit ball.
    """
    M = 2*np.random.rand(n,n+1) + 2.j*np.random.rand(n,n+1) - (1+1.j)*np.ones([n,n+1])
    return M

def phi(M, dtype=np.cfloat):
    """
    Compute a unitary matrix that satisfies M*phi(M)*e_0 == 0.
    Currently, phi() is not guaranteed to be a.e. continuous.
    """
    # validate input
    if type(M) != np.ndarray:
        raise TypeError("The input M must be of type numpy.float.")
    d0, d1 = np.shape(M)
    if d0+1 != d1:
        raise ValueError("The input must be of shape [n,n+1].")
    dim = d1
    # add patch e_0 to matrix M
    M_patch = np.zeros([dim,dim], dtype = dtype)
    M_patch[:-1,:] = M
    M_patch[-1,0] = 1
    # get a kernel vector
    z_patch = np.zeros(dim, dtype = dtype)
    z_patch[-1] = 1
    x = np.linalg.solve(M_patch, z_patch).reshape(dim)
    # compute phi(M)
    A = np.eye(dim, dtype = dtype)
    A[:,0] = x
    Q, _ = np.linalg.qr(A, mode='complete')
    return Q

def efficient_initial_pair(symbols, d, t, h, M):
    """
    Construct the efficient initial pair $(g, e_0)$ following Beltrán and Pardo.
    """
    n = len(symbols)-1
    N = bs.dim_Hd(n, d)
    sqrt_d = d**0.5
    norm_h = bs.canonical_norm(h)
    C = (M @ phi(M))[:,1:]

    tau = ((n**2 + n) / N)**0.5
    lambda1 = (1 - tau**2 * t ** (1/(n**2 + n)))**0.5
    lambda2 = tau * t ** (1/(2*n**2 + 2*n))

    g1 = np.zeros(n, dtype=object)
    g2 = np.zeros(n, dtype=object)

    # determine the coefficients of x_j-higher-degree, j>0:
    for i, h_ in enumerate(h):
        h_dict = h_.as_dict()
        for (alpha, coeff) in h_dict.items():
            h_dict[alpha] = lambda1 * (factorial(d[i]) / np.prod(factorial(alpha)))**0.5 * coeff / norm_h
        g1[i] = sy.Poly(h_dict, symbols)

    # determine the x_j-linear coefficients, j>0:
    for i, row in enumerate(C):
        coeff_dict = {}
        alpha = bs.e0(n)
        alpha[0] *= d[i]-1
        for j, coeff in enumerate(row):
            j += 1
            alpha[j] += 1
            coeff_dict[tuple(alpha)] = lambda2 * sqrt_d[i] * coeff / np.linalg.norm(M)
            alpha[j] -= 1
        g2[i] = sy.Poly(coeff_dict, symbols)

    g = g1 + g2
    return g
