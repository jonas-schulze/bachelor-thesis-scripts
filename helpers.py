class DefaultList(list):
    def __copy__(self):
        return []

def next_permutation(l):
    """
    Changes a list to its next permutation, in place.
    Returns true unless wrapped around so result is lexicographically smaller.
    
    Props to shreevatsa: https://github.com/shreevatsa/misc-math/blob/master/pypermutations.py
    """
    n = len(l)
    #Step 1: Find tail
    last = n-1 #tail is from `last` to end
    while last>0:
        if l[last-1] < l[last]: break
        last -= 1
    #Step 2: Increase the number just before tail
    if last>0:
        small = l[last-1]
        big = n-1
        while l[big] <= small: big -= 1
        l[last-1], l[big] = l[big], small
    #Step 3: Reverse tail
    i = last
    j = n-1
    while i < j:
        l[i], l[j] = l[j], l[i]
        i += 1
        j -= 1
    return last>0
